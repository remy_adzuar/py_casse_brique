class Score:

    def __init__(self) -> None:
        self.scores = []

    def read_file_score(self):
        file = open("./scores.txt", "r")
        self.scores = []
        for line in file:
            strip = line.strip()
            split = line.split()
            try:
                int(split[1])
            except:
                print("LE SCORE n'est pas un entier....")
                break
            self.scores.append(split[0:2])
        file.close()

    def get_scores(self):
        return self.scores
    
    def sort_scores(self):
        self.scores.sort(key= lambda x:int(x[1]), reverse=True)
    
    def get_min_score(self):
        self.sort_scores()
        if len(self.scores)>0:
            mini = self.scores[-1]
    
    def append_score(self, score):
        if len(self.scores)< 10:
            self.scores.append(score)
        else:
            mini = self.get_min_score()
            if mini[1] < score[1]:
                self.scores.pop(-1)
                self.scores.append(score)

        self.sort_scores()
    
    def can_append_score(self, score):
        if len(self.scores) < 10:
            return True
        else:
            mini = self.get_min_score()
            if mini[1] < score[1]:
                return True
        return False
    
    def write_score_file(self):
        file = open("./scores.txt", "w")
        self.sort_scores()
        for line in self.scores:
            s = str(line[0]+" "+str(line[1])+"\n")
            file.write(s)
        file.close()
    

if __name__ == "__main__":

    S = Score()
    S.read_file_score()
    print(S.scores)
    S.get_min_score()
    
    S.sort_scores()

    score = ["TEST", 44]
    S.append_score(score)
    S.write_score_file()
    