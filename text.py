import pygame
from config import Config

class Text:

    def __init__(self, config:Config, pos, content:str="", center:bool=False) -> None:
        self.config = config
        self.pos = pos
        self.content = content
        self.center = center
        self._highlight = False
        self.tx_color = (255,255,255)
        self.bg_color = (100,100,100)
        self.load_text()
    
    def set_tx_color(self, color):
        self.tx_color = color
    
    def set_bg_color(self, color):
        self.bg_color = color
    
    def flip_highlight(self):
        if self._highlight:
            self._highlight = False
            self.tx_color = (255,255,255)
            self.bg_color = (100,100,100)
            self.load_text()
        else:
            self._highlight = True
            self.tx_color = (255,0,0)
            self.bg_color = (255,255,255)
            self.load_text()

    def load_text(self):
        font = self.config.font
        rendu = font.render(self.content, True, self.tx_color, self.bg_color)
        self.rect = rendu.get_rect()
        if self.center:
            self.rect.center = self.pos
        else:
            self.rect.topleft = self.pos
        self.rendu = rendu
    
    def print_text(self):
        screen = self.config.screen
        screen.blit(self.rendu, self.rect)