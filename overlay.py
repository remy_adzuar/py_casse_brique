import pygame
from config import Config
from entity import Ball
from text import Text
from grid import Grid
from entity import Brick

class Overlay:

    def __init__(self, config:Config, rect:pygame.Rect) -> None:
        self.rect = rect
        self.config = config
        self.color = (0,255,0)
    
    def draw(self):

        pygame.draw.rect(self.config.screen, self.color, self.rect)


class Line_overlay:

    def __init__(self, config:Config, ball:Ball) -> None:
        self.config = config
        self.ball = ball
    
    def draw_predict(self):

        bcx = self.ball.rect.centerx
        bcy = self.ball.rect.centery

        dir_x = self.ball._direction[0]
        dir_y = self.ball._direction[1]

        len_x = bcx-1200
        if dir_x >= 0:
            len_x = 1200-bcx
        if self.ball._direction[0] != 0:
            len_x = len_x // self.ball._direction[0]
        
        len_y = bcy-680
        if dir_y >= 0:
            len_y = 680- bcy
        if self.ball._direction[1] != 0:
            len_y = len_y // self.ball._direction[1]
        
        lenght = min(len_y, len_x)

        pygame.draw.line(self.config.screen, (0,255,0),(bcx, bcy), 
        (bcx+dir_x*lenght,
        bcy+dir_y*lenght))

class Overlay_timer:

    def __init__(self, config:Config, second) -> None:
        self.millisecond = second*1000
        self.start = pygame.time.get_ticks()
        self.config = config
        self.rect = pygame.Rect(600,700,320,15)
    
    def draw(self):
        pygame.draw.rect(self.config.screen, (0,0,255), self.rect)
    
    def update(self):
        seconds = (pygame.time.get_ticks()-self.start)
        size = (1-(seconds/self.millisecond))*320
        self.rect = pygame.Rect(600,700, size, 15)

class Grid_miniature:

    def __init__(self, config:Config, grid:Grid, pos) -> None:
        self.config = config
        self.grid = grid
        self.pos = pos
    
    def draw(self):

        i = 0
        for line in self.grid.matrix:
            j = 0
            for c in line:
                if isinstance(c, Brick):
                    color = c.color
                    pygame.draw.rect(self.config.screen, color, (self.pos[0]+(4*j),self.pos[1]+(2*i),4,2))


                j+= 1
            i += 1


# a deplacer
def print_score(config, score, nb_brick, nb_balle):
    string_text = "Score : "+str(score)+" | briques : "+str(nb_brick)+" | Balles : "+str(nb_balle)
    text = Text(config, (40,700),string_text,False)
    text.print_text()

