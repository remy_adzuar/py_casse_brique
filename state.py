import pygame
import keypressed

from text import Text
from config import Config
from game import Game
from scores import Score

from grid import Grid
from overlay import Grid_miniature

class State:

    def __init__(self, config:Config) -> None:
        self.config = config
        self.status = "MENU"
        self.selected = None

        root = "./levels/"
        self.level_list = [
            root+"1.txt",
            root+"2.txt",
            root+"3.txt"
        ]

    ## MENU PRINCIPAL DU JEU
    def menu(self):

        self.texts = []

        self.texts.append(Text(self.config, (128,128), "1. Nouvelle Partie"))
        self.texts.append(Text(self.config, (128,160), "2. Choix Niveau"))
        self.texts.append(Text(self.config, (128,192), "3. Tableau de Score"))
        self.texts.append(Text(self.config, (128,224), "4. Aide / Manuel"))
        self.texts.append(Text(self.config, (128,256), "5. Credits"))
        self.texts.append(Text(self.config, (128,288), "6. Quitter"))
        
        self.selected = 0
        self.texts[self.selected].flip_highlight()
        self.selected_return = ["NEWGAME","LEVELCHOICE","SCORE","HELP","CREDITS","QUIT"]

        loop = True
        while loop:
            self.config.screen.fill((0,0,0))

            result = keypressed.get_key_pressed()
            if result == pygame.QUIT:
                self.status = "QUIT"
                loop = False
            if result == pygame.K_DOWN or result == pygame.K_UP:
                self.get_selected_text().flip_highlight()
                
                # Oui j'ai ose
                self.selected += 1
                if result == pygame.K_UP:
                    self.selected -= 2
                
                self.selected = self.selected%len(self.texts)
                self.get_selected_text().flip_highlight()

            if result == pygame.K_RETURN:
                loop = False
                self.status = self.selected_return[self.selected]
            
            self.get_selected_cursor()
            if pygame.mouse.get_pressed()[0]:
                loop = False
                self.status = self.selected_return[self.selected]

            self.print_text()

            pygame.display.update()
    

    ## NOUVELLE PARTIE
    def new_game(self):

        ## Ecran Avant Jeu
        self.texts = []
        self.texts.append(Text(self.config,(128,128),"NOUVELLE PARTIE"))
        self.texts.append(Text(self.config,(128,160),"APPUYEZ SUR UNE TOUCHE POUR COMMENCER"))

        loop = True
        while loop:
            self.config.screen.fill((0,0,0))

            result = keypressed.get_key_pressed()
            if result == pygame.QUIT:
                self.status = "QUIT"
                loop = False
            elif result != None:
                loop = False
            elif pygame.mouse.get_pressed()[0]:
                loop = False

            self.print_text()
            pygame.display.update()

        ## Debut du jeu

        self.texts = []

        i = 0
        bonus = 0
        while self.status == "NEWGAME":
            if i >= len(self.level_list):
                print("VICTOIRE TOUT NIVEAUX")
                self.status = "VICTORY"
            else:
                game = Game(self.config, self.level_list[i])    
                if game.get_state() == "DEAD" or game.get_state() == "FAIL":
                    print("RETOUR MENU")
                    self.status = "MENU"
                elif game.get_state() == "VICTORY":
                    print("PASSAGE NIVEAU SUIVANT")
                    bonus += game.racket.bonus
                    print(bonus)
                    i += 1
                else:
                    print("ERREUR, retour menu")
                    self.status = "MENU"
        
        return bonus

    ## CHOIX DU NIVEAU DE DEPART
    def level_choice(self):

        self.texts = []
        self.texts.append(Text(self.config, (128,128), "LEVEL 1"))
        self.texts.append(Text(self.config, (128,160), "LEVEL 2"))
        self.texts.append(Text(self.config, (128,200), "LEVEL 3"))
        self.texts.append(Text(self.config, (128,232), "Retour MENU"))

        self.selected = 0
        self.texts[self.selected].flip_highlight()
        self.selected_return = ["LEVEL_1","LEVEL_2","LEVEL_3","MENU"]

        mouse_relapsed = False

        grid1 = Grid(self.config)
        grid1.load_grid_from_file("./levels/1.txt")
        grid2 = Grid(self.config)
        grid2.load_grid_from_file("./levels/2.txt")
        grid3 = Grid(self.config)
        grid3.load_grid_from_file("./levels/3.txt")

        grid_miniature1 = Grid_miniature(self.config,grid1, (256,100))
        grid_miniature2 = Grid_miniature(self.config,grid2, (256,160))
        grid_miniature3 = Grid_miniature(self.config,grid3, (256,200))

        loop = True
        while loop:

            self.config.screen.fill((0,0,0))

            result = keypressed.get_key_pressed()
            if result == pygame.QUIT:
                self.status = "QUIT"
                loop = False
            self.move_selected(result)

            self.get_selected_cursor()
            if not pygame.mouse.get_pressed()[0]:
                mouse_relapsed = True
            if pygame.mouse.get_pressed()[0] and mouse_relapsed:
                loop = False
                level = self.selected_return[self.selected]
        
                if level == "LEVEL_1":
                    game = Game(self.config, "./levels/1.txt")
                elif level == "LEVEL_2":
                    game = Game(self.config, "./levels/2.txt")
                elif level == "LEVEL_3":
                    game = Game(self.config, "./levels/3.txt")
                elif level == "MENU":
                    self.status = "MENU"

                loop = False
                self.status = "MENU"

            grid_miniature1.draw()
            grid_miniature2.draw()
            grid_miniature3.draw()
            self.print_text()
            pygame.display.update()
    
    def help(self):
        # Screen listant les commande du jeu

        self.texts= []
        self.texts.append(Text(self.config, (128,128), "AIDE :"))
        self.texts.append(Text(self.config, (128,160), "1. Appuyer sur Espace pour lancer une nouvelle balle"))
        self.texts.append(Text(self.config, (128,192), "2. La couleur des brique indique leur solidite, elle varie de 1 a 4"))
        self.texts.append(Text(self.config, (128,224), "3. Une brique detruite fait tomber un bonus qu'il faut ramasser avec la raquette"))
        self.texts.append(Text(self.config, (128,256), "4. Le score n'est enregistre que dans les parties normales"))
        self.texts.append(Text(self.config, (128,288), "5. Le score n'est pas enregistre si vous choississez le niveau"))
        self.texts.append(Text(self.config, (128,320), "Appuyer sur une touche pour revenir au menu"))

        loop = True
        while loop:
            self.config.screen.fill((0,0,0))

            result = keypressed.get_key_pressed()
            if result == pygame.QUIT:
                self.status = "MENU"
                loop = False
            if result != None:
                self.status = "MENU"
                loop = False

            self.print_text()

            pygame.display.update()
    
    def score(self, score_result=None):

        self.texts = []
        self.texts.append(Text(self.config, (128,128), "TABLEAU SCORE"))

        scores = Score()
        scores.read_file_score()
        i = 0
        for score in scores.get_scores():
            self.texts.append(Text(self.config, (128,160+i*32), str(i+1)+":"+score[0]+ ":"+ score[1]))
            i += 1

        name = ""

        if score_result:
            print("RESULTAT")
            print(score_result)

        loop = True
        while loop:
            self.config.screen.fill((0,0,0))
            name_txt = Text(self.config, (128,640),"Entrez votre nom >" + name + "_")

            result = keypressed.get_unicode()
            if result != None and result.type == pygame.QUIT:
                self.status = "MENU"
                loop = False
            if score_result == None:
                if result != None:
                    self.status = "MENU"
                    loop = False
            else:
                if score_result != None and result != None:
                    # ENTRER SON NOM AU TABLEAU DE SCORE
                    try:
                        if result.key == pygame.K_RETURN:
                            if len(name)>2:
                                print([name, score_result])
                                scores.append_score([name, score_result])
                                scores.write_score_file()
                                self.status = "MENU"
                                loop = False
                            else:
                                print("NOM TROP COURT")
                    except:
                        print("PROBLEM")
                        break
                    else:
                        try:
                            if result.key == pygame.K_BACKSPACE:
                                name = name[:-1]
                            elif result.key == pygame.K_RETURN:
                                pass
                            else:
                                char = result.unicode
                                name = name + char
                                print(name, ">>", char)
                        except:
                            pass

            # Besoin de mettre la logique pour entrer un score
            if score_result != None:
                name_txt.print_text()
            self.print_text()

            pygame.display.update()
    
    def credits(self):

        self.texts = []
        self.texts.append(Text(self.config, (128,128), "CREDITS :"))
        self.texts.append(Text(self.config, (128,160), "Developpeur : ADZUAR REMY"))
        self.texts.append(Text(self.config, (128,192), "Studio : HOMEMADE"))

        loop = True
        while loop:

            self.config.screen.fill((0,0,0))

            result = keypressed.get_key_pressed()
            if result == pygame.QUIT:
                self.status = "QUIT"
                loop = False
            if result != None:
                self.status = "MENU"
                loop = False

            self.print_text()

            pygame.display.update()

    def victory(self):

        self.texts = []
        self.texts.append(Text(self.config,(128,128), "!BRAVO!"))
        self.texts.append(Text(self.config,(128,160), "VOUS AVEZ GAGNE"))

        loop = True
        while loop:

            self.config.screen.fill((0,0,0))

            result = keypressed.get_key_pressed()
            if result == pygame.QUIT:
                self.status = "QUIT"
                loop = False
            if result != None:
                self.status = "SCORE"
                loop = False

            self.print_text()

            pygame.display.update()

    ## FONCTIONS UTILITAIRES DE LA CLASSE

    ## FONCTIONS UTILITAIRES DE LA CLASSE

    ## FONCTIONS UTILITAIRES DE LA CLASSE

    def move_selected(self, result):
        if result == pygame.K_DOWN or result == pygame.K_UP:
            self.get_selected_text().flip_highlight()
        
            # Oui j'ai ose
            self.selected += 1
            if result == pygame.K_UP:
                self.selected -= 2
            
            self.selected = self.selected%len(self.texts)
            self.get_selected_text().flip_highlight()
    
    def print_text(self):
        for text in self.texts:
            if isinstance(text, Text):
                text.print_text()

    def get_status(self):
        return self.status
    
    def set_status(self, value:str):
        self.status = value
    
    def get_selected_text(self):
        text = self.texts[self.selected]
        if isinstance(text, Text):
            return text
    
    def get_selected_cursor(self):
        i = 0
        cursor_pos = pygame.mouse.get_pos()
        for text in self.texts:
            if isinstance(text, Text):
                if text.rect.collidepoint(cursor_pos):
                    self.get_selected_text().flip_highlight()
                    self.selected = i
                    self.get_selected_text().flip_highlight()
            i += 1