import pygame
from game import Game
from config import Config
from state import State

def main():

    config = Config()

    # game:Game = Game(config)
    state = State(config)
    score = 0

    while True:

        if state.get_status() == "MENU":
            state.menu()

        elif state.get_status() == "QUIT":
            break

        elif state.get_status() == "NEWGAME":
            score = state.new_game()

        elif state.get_status() == "LEVELCHOICE":
            state.level_choice()

        elif state.get_status() == "HELP":
            state.help()
        
        elif state.get_status() == "SCORE":
            if score>0:
                state.score(score)
            else:
                state.score(None)
            score = 0
        
        elif state.get_status() == "CREDITS":
            state.credits()

        elif state.get_status() == "VICTORY":
            state.victory()

        else:
            print("ERROR")
            state.menu()

    pygame.quit()

    # status = game.get_state()
    # if status == "FAIL":
    #     print("Vous n'avez plus de balle pour jouer")
    # if status == "DEAD":
    #     print("Vous avez clique sur la croix rouge")
    # if status == "VICTORY":
    #     print("Victoire, vous avec enleve toute les briques")

if __name__ == "__main__":

    pygame.init()

    main()
