import pygame


def get_key_pressed():
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            return event.type
        if event.type == pygame.KEYDOWN:
            return event.key

def get_unicode():
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            return event
        if event.type == pygame.KEYDOWN:
            return event