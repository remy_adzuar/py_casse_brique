import pygame
from config import Config

import keypressed

from grid import Grid
from entity import Entity, Ball, Racket, Brick, Item

from overlay import Overlay, Line_overlay, Overlay_timer, print_score as Overlay_score


class Game:

    def __init__(self, config:Config, file:str) -> None:
        
        self.config = config

        self.entities:Entity = []
        self.grid = Grid(self.config)
        self.nb_ball = 3
        self.nb_brick = 0
        # self.grid.test_grid()
        # self.grid.test_full_grid()
        self.grid.load_grid_from_file(file)
        self.overlays = []
        self.overlays_show = False

        self.load(config)
        self.state = "ALIVE"

        self.test_timer = Overlay_timer(self.config, 10)

        while self.state == "ALIVE":

            self.update()
            self.draw()
            self.key_pressed()
        
        
        
    # Chargement du jeu
    def load(self, config:Config):

        self.create_ball()

        #test overlay de balle
        self.ball_overlay = Line_overlay(self.config, self.ball)

        self.racket = Racket(self.config, pygame.Rect(1200//2-32,680,80,16),(0,255,0))
        self.entities.append(self.racket)

        ## Chargement de la grid de brick
        self.grid.load_entity_to_list(self.entities)
        self.count_brick()

    # Update du jeu
    def update(self):
        self.config.clock.tick(60)

        for entity in self.entities:
            entity.update()
        for entity in self.entities:
            res = self.ball.collide_with_entity(entity)
            if res:
                self.overlays.append(Overlay(self.config,
                pygame.Rect(self.ball.rect.x-1,self.ball.rect.y-1,3,3)))
                self.config.play_sound("brick_rebound")
                break
            self.racket.check_collide_item(entity)
        
        for i in range(len(self.entities)-1,-1,-1):
            if isinstance(self.entities[i], Brick):

                res = self.remove_entity(self.entities[i], i)
                if res:
                    self.remove_one_brick()

            elif isinstance(self.entities[i], Ball):
                self.remove_entity(self.entities[i], i)
            elif isinstance(self.entities[i], Item):
                self.remove_entity(self.entities[i],i)
        
        self.test_timer.update()

    # Dessin du jeu
    def draw(self):
        self.config.screen.fill((0,0,0))
        # Dessin des entities
        for entity in self.entities:
            pygame.draw.rect(self.config.screen, entity.color,entity.rect)


        
        Overlay_score(self.config, self.racket.bonus, self.nb_brick, self.nb_ball) # a deplacer
        self.test_timer.draw()

        if self.overlays_show:
            self.ball_overlay.draw_predict()
            for overlay in self.overlays:
                if isinstance(overlay, Overlay):
                    overlay.draw()

        pygame.display.update()


    # Gestion input jeu
    def key_pressed(self):
        result = keypressed.get_key_pressed()
        if result == pygame.QUIT:
            self.state = "DEAD"
        if result == pygame.K_SPACE:
            if self.ball._direction == [0,0]:
                self.ball.set_direction([0,-5])
        if result == pygame.K_t:
            print(self.print_all_ball())
            self.state = "VICTORY"
        if result == pygame.K_o:
            ## Active / Desactive l'overlay de la balle
            if self.overlays_show:
                self.overlays_show = False
            else:
                self.overlays_show = True
    


    ## Separation

    ##

    ##

    ##

    ## Fonction utilitaire pour la logique du jeu

    def get_state(self):
        return self.state

    def remove_entity(self,entity,pos):
        # Retire une entite de la liste des entites si elle n'a plus de HP
        if entity.hp <= 0:
            self.entities.pop(pos)
            if isinstance(entity, Brick):
                item = entity.get_item()
                if item != None:
                    self.entities.append(item)
                    item.drop()
                    return True
            if isinstance(entity, Ball):
                self.destroy_ball()
    
    ## Gestion des balles

    def create_ball(self):
        self.ball = Ball(self.config, pygame.Rect(1200//2,680-16,16,16),(255,255,255))
        self.entities.append(self.ball)
    
    def destroy_ball(self):
        self.nb_ball -= 1
        if self.nb_ball > 0:
            self.create_ball()
        else:
            self.state = "FAIL"
    
    def print_all_ball(self):
        for entity in self.entities:
            if isinstance(entity, Ball):
                print(Ball)
    
    ## Gestion des briques

    def count_brick(self):
        for entity in self.entities:
            if isinstance(entity, Brick):
                self.nb_brick += 1
    
    def remove_one_brick(self):
        self.nb_brick -= 1
        self.check_victory()
    
    def check_victory(self):
        if self.nb_brick <= 0:
            self.count_brick()
            print("Verif nb brick")
            print(self.count_brick())
            self.state = "VICTORY"
        