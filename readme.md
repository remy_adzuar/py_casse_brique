# Projet Casse Brique

Implementer un Casse Brique avec Python3 et Pygame

## Fonctionnalites

Le joueur controle une raquette avec laquelle il renvoie une balle
La balle peut heurter et rebondir sur les murs Gauche, Droit et Haut
La balle rebondit sur les briques en leur infligeant des degats
Si la balle tombe en bas la partie est perdue
Si il ne reste plus de brique, la partie est gagnee

## Prevision

10 min de preparation papier
10 min de preparation PC

16 x 25 min pour aboutir a un prototype fonctionnel
16 x 25 min pour paufiner le prototype

## Idees

Avoir des niveaux,
Avoir plusieurs balles
Avoir plusieurs type de brique de resistance et/ou de physique differente
Avoir des bonus malus a ramasser avec la raquette :
	Raquette plus large/petite
	Changement de physique de la balle
	Acceleration/ralentissement de la balle/raquette
	Inversion de commande
	Raquette collante
Faire des niveaux proceduraux
Enregistrer des scores
Pouvoir selectionner son niveau

## Suivis Projet

1 ere session
25 min, Mise en place des Objets et de la structure de code
5 min pause,
25 min, Premiere boucle de jeu OK, Debut de hierarchie de classe pour les entities
fin session

2 eme session
25 min, Premier test avec le systeme d'heritage, update() dans entity et ses classe enfants
5 min pause,
25 min, Collision balle et racket ok, avec rebond sur les murs et la racket
fin session

3 eme session
25 min, Debut programmation de la classe grid qui contient des brick
5 min pause,
25 min, Ajout des HP pour les brick et detection de collisions, amelioration rebond racket
fin session

4 eme session
25 min, Ajout collision sur les briques avec la balle
5 min pause,
25 min, Ajout lecture de fichier de niveau, et test de mecanisme de conversation de l'energie
fin session

22/12/2022
5 eme session
25 min, Ajout decompte du nombre de balle,
5 min pause,
25 min, Changement de la taille de la grille, modif des collision, ajout partiel de la logique de victoire
5 min pause,
25 min, ajout classe pour gerer le Text et une classe pour gerer les states
5 min pause,
25 min, ajout state MENU, NEW_GAME, et debut ajout LEVEL_CHOICE
fin session

6 eme session
25 min, Test UI Clickable, Changement des input dans le menu au projet de la souris, highlight de la ligne selected
5 min pause,
25 min, Premier test Ajout des bonus dans les briques
5 min pause,
25 min, Ajout de drop de bonus pour ameliorer le score
5 min pause,
25 min Ajout de choix du niveau de jeu
Fin de session

Fin du prototype fonctionnel

## Observation

Le prototype est fonctionnel, pour le peaufinage je pense mettre l'accent sur l'ergonomie globale, et faire un tableau de highscore proprement

23/12/2022
7 eme session
25 min, Debug sur les states
5 min pause,
25 min, Ajout state manquants, scores, aide, credits, victoire, modif tableau de score
5 min pause,
25 min, Ajout de classe Scores
5 min pause, 
25 min Ajout du score en cours dans le state de score
Fin de session

8 eme session
25 min, Ajout du nom dans le tableau de score ok
5 min pause,
25 min ajout de different type de brique
5 min pause,
25 min, Bug diagonale resolu, suivres la raquettte avec la balle avant le coup d'envoie
empecher le rappui sur espace, ajout d'un essais de visualisation de la trajectoire de la balle
5 min pause,
25 min, Debut d'ajout des sons
Fin de session, Reste 8 * 25min pour boucler le peaufinage

24/12/2022
9 eme session
25 min, Modif des overlays, remplissage de l'ecran d'aide
5 min pause,
25 min, Test Ajout de miniature dans le choix du niveau
fin de session

10 eme session
15 min, Ajout de toute les miniature, correction bug mineure. Arret de la production
Fin de projet

## Observation Fin de projet

La production du casse brique a beneficier des erreur commise sur le precedent jeu : Master Mind
En consequence sa production a ete un peu plus rapide que prevue, neanmoins le code presente encore des difficultes
et je dois trouver des solutions. La hierarchie de classe dans Entite fonctionne plutot bien, mais je n'exploite pas assez
les fonction draw de cette classe. L'import de grid a partir de fichier texte permet de fabriquer des niveaux assez facilement.
Il faudrait que j'arrive a supprimer les boucle dans les fonction update, et draw de la classe game, pour ca on va essayer
de creer des classe manager (entity manager, draw Manager), le systeme de state fonctionne plutot bien mais meriterait d'etre 
mieux formalise

## Budget temps :
Reste 2.75h de budget