import pygame
from config import Config
from entity import Entity, Brick, Item

class Grid:

    def __init__(self, config:Config) -> None:
        self.config = config
        self.matrix = [[None for i in range(18)] for i in range(15)]
        self.size = [64,32]
    
    def test_grid(self):
        self.set_pos((0,0))
        self.set_pos((1,1))
        self.set_pos((2,2))
        self.set_pos((3,3))
        self.set_pos((4,4))
        self.set_pos((5,5))
    
    def test_full_grid(self):
        i = 0
        while i < len(self.matrix):
            j = 0
            while j < len(self.matrix[i]):
                self.set_pos((j,i))
                j += 1
            i += 1

    def get_pos(self, pos):
        x = pos[0]
        y = pos[1]
        return self.matrix[y][x]
    
    def set_pos(self,pos, hp:int=1):
        x = pos[0]
        y = pos[1]
        sizex = self.size[0]
        sizey = self.size[1]
        value = pygame.Rect(x*sizex,y*sizey,sizex-1,sizey-1)
        self.matrix[y][x] = Brick(self.config, value, hp)
    
    def remove_pos(self, pos):
        x = pos[0]
        y = pos[1]
        self.matrix[y][x] = None

    def load_entity_to_list(self, entities):
        for line in self.matrix:
            for case in line:
                if isinstance(case, Entity):
                    entities.append(case)
    
    def load_grid_from_file(self, file):
        f = open(file, "r")
        i = 0
        for line in f:
            cases = line.strip().split(',')
            j = 0
            while j < len(cases):
                case = cases[j]
                if case == "1":
                    self.set_pos((j,i))
                    brick = self.get_pos((j,i))
                    if isinstance(brick, Brick):
                        brick.set_item()
                elif case == "2":
                    self.set_pos((j,i),2)
                    brick = self.get_pos((j,i))
                    if isinstance(brick, Brick):
                        brick.set_item()
                elif case == "3":
                    self.set_pos((j,i),3)
                    brick = self.get_pos((j,i))
                    if isinstance(brick, Brick):
                        brick.set_item()
                elif case == "4":
                    self.set_pos((j,i),4)
                    brick = self.get_pos((j,i))
                    if isinstance(brick, Brick):
                        brick.set_item()
                j+= 1
            i += 1


if __name__ == "__main__":

    G = Grid()
    G.load_grid_from_file("./levels/1.txt")
    print(G.matrix)