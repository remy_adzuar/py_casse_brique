import pygame
class Config:

    def __init__(self) -> None:
        self.screen:pygame.Surface = pygame.display.set_mode((1280,720))
        self.offset = [0,0]
        self.clock = pygame.time.Clock()
        self.font = pygame.font.Font("freesansbold.ttf",16)
        root_dir = "./sounds/"
        self.sounds = {
        }
        self.images = {

        }
        self.add_sound("ball_fall", root_dir+"ball_fall.wav")
        self.add_sound("bonus", root_dir+"bonus.wav")
        self.add_sound("brick_rebound", root_dir+"brick_rebound.wav")
        self.add_sound("rebound", root_dir+"rebound.wav")
    
    def add_sound(self, name, URL):
        self.sounds[name] = pygame.mixer.Sound(URL)

    def play_sound(self, name):
        pygame.mixer.Sound.play(self.sounds[name])