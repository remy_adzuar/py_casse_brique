import pygame
from config import Config


class Entity:

    def __init__(self, config:Config, rect:pygame.Rect) -> None:
        self.config = config
        self.rect = rect
        self.color = (128,128,128)
        self._direction = [0,0]
        self.hp = 1
    
    def update(self):
        self.move()

    def set_direction(self, direction):
        self._direction = direction
    
    def get_direction(self):
        return [self._direction[0], self._direction[1]]

    def move(self):
        self.rect.x += self._direction[0]
        self.rect.y += self._direction[1]

    def set_pos(self, pos):
        self.rect.x = pos[0]
        self.rect.y = pos[1]

    def invert_direction(self, x:bool=False, y:bool=False):
        if x:
            self._direction[0] = self._direction[0] * -1
        if y:
            self._direction[1] = self._direction[1] * -1
    
    def take_damage(self, amount:int=-1):
        self.hp += amount

class Ball(Entity):
    def __init__(self, config:Config, rect: pygame.Rect, color) -> None:
        super().__init__(config, rect)
        self.color = color
        self.direction = [5,-5]
    
    def update(self):
        super().move()
        if self._direction == [0,0]:
            self.rect.centerx = pygame.mouse.get_pos(0)[0]
        self.check_borders()

    def collide_with_entity(self, entity):
        if self != entity:

            # Collide avec la racket
            if isinstance(entity, Racket):
                if self.rect.colliderect(entity):
                    delta = self.rect.centerx - entity.rect.centerx
                    if delta >= 0:
                        power = delta // 5
                    if delta < 0:
                        power = delta // 5
                    if self._direction[1] > 0:
                        self._direction[0] = power
                        if self._direction[1] > 0:
                            self.invert_direction(False, True)
            if isinstance(entity, Brick):
                return self.check_collision_brick(entity)
                    
    
    def check_borders(self):

        if self.rect.x < 0 or self.rect.x >= 1200-self.rect.width:
            if self.rect.x < 0 :
                self.rect.x = 0
            if self.rect.x > 1200-self.rect.width:
                self.rect.x = 1200-self.rect.width
            self.invert_direction(True,False)

        if self.rect.y < 0 and self._direction[1] < 0:
            self.rect.y = 0
            self.invert_direction(False, True)
        
        if self.rect.y >700:
            self.take_damage()
    
    def check_collision_brick(self, entity):
        if isinstance(entity, Brick):
            if self.rect.colliderect(entity):
                cx = self.rect.centerx
                cy = self.rect.centery

                if cx > entity.rect.x and cx < entity.rect.x + entity.rect.width:
                    # La balle arrive soit d'en haut ou d'en bas
                    self.invert_direction(False, True)

                elif cy > entity.rect.y and cy < entity.rect.y + entity.rect.height:
                    # La balle arrive soit de la droite, soit de la gauche
                    self.invert_direction(True, False)

                else:
                    ## collision avec centre de la balle en dehors
                    left = self.rect.x
                    right = self.rect.x+self.rect.width
                    # test si la gauche de la balle est inferieur a la droite de la brique
                    # et si la droite de la balle est superieur a la gauche de la brique :3
                    if left < entity.rect.x+entity.rect.width and right > entity.rect.x:
                        self.invert_direction(False, True)
                    top = self.rect.y
                    bottom = self.rect.y+self.rect.height
                    if top < entity.rect.y+entity.rect.height and bottom > entity.rect.y:
                        self.invert_direction(True,False)

                entity.take_damage()
                return True
        return False


class Racket(Entity):

    def __init__(self, config:Config, rect: pygame.Rect, color) -> None:
        super().__init__(config, rect)
        self.color = color
        self.bonus = 0

    def update(self):
        # super().update()
        self.move()
        self.check_borders()
    
    def move(self):
        if pygame.mouse.get_focused():
            mouse_pos = pygame.mouse.get_pos()
            self.rect.x = mouse_pos[0]-self.rect.width//2
    
    def check_borders(self):
        if self.rect.x < 0:
            self.rect.x = 0
        if self.rect.x > 1200-self.rect.width:
            self.rect.x = 1200-self.rect.width
    
    def check_collide_item(self, item):
        if isinstance(item, Item):
            if self.rect.colliderect(item):
                self.bonus += item.get_bonus()

class Item(Entity):

    def __init__(self, config:Config, rect: pygame.Rect, name) -> None:
        super().__init__(config,rect)
        self.name = name
        self.color = (255,215,0)
        self._direction=[0,0]
        self.droped = False
        self.bonus = 1
    
    def move(self):
        super().move()
    
    def update(self):
        super().update()
        self.check_border()

    def drop(self):
        if self.droped == False:
            self.droped = True
            self._direction = [0,4]
        
    def check_border(self):
        if self.rect.y >= 700:
            self.take_damage()
    
    def get_bonus(self):
        self.take_damage()
        return self.bonus


class Brick(Entity):

    def __init__(self, config:Config, rect: pygame.Rect, hp:int=1) -> None:
        super().__init__(config, rect)
        self.hp = hp
        self.color = (255,0,0)
        if hp == 2:
            self.color = (128,0,255)
        elif hp == 3:
            self.color = (0,0,255)
        elif hp == 4:
            self.color = (0,128,128)
        self._item = None
    
    def update(self):
        return super().update()
    
    def take_damage(self, amount: int = -1):
        super().take_damage(amount)
        if self.hp == 1:
            self.color = (255,0,0)
        if self.hp == 2:
            self.color = (128,0,255)
        if self.hp == 3:
            self.color = (0,0,255)

    def set_item(self):
        rect = pygame.Rect(self.rect.centerx-4,self.rect.centery-4,4,4)
        item = Item(self.config, rect, "TEST")
        self._item = item

    def get_item(self):
        return self._item
